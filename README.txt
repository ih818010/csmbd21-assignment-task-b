This repository contains the code for executing a map reduce job using python.

The repository is structured as following - 
+============================
|-- data
|
|-- src
|   |-- common (Common Templates)
|   |   |-- mapper.py
|   |   |-- shuffler.py
|   |   |-- reducer.py
|   |
|   |-- dropduplicates (Drop Duplicates Map-Reduce Pipeline)
|   |   |-- mapper.py
|   |   |-- shuffler.py
|   |   |-- reducer.py
|   |
|   |-- groupby (GroupBy Map-Reduce Pipeline)
|   |   |-- mapper.py
|   |   |-- shuffler.py
|   |   |-- reducer.py
|   |
|   |-- max (Max Map-Reduce Pipeline)
|   |   |-- mapper.py
|   |   |-- shuffler.py
|   |   |-- reducer.py
+============================

Steps - 
    1. Create a new virtual environment for python and activate. (Optional but highly recommended) 
    2. Install requirements specified in the accompanying requirements.txt file
    3. Run `sh max_trips.sh` in the shell.

Overview
    - The file is read using the `cat` command.
    - The output of cat is pipelined to the drop duplicates job. Here the key-value pair is formed by the hash of the row string
      and the entire row itself.
    - The output of drop duplicates is pipelined to the groupby and count job. Here the key-value pair is formed by the passenger_id
      and the number of flights taken by each passenger.
    - The output of groupby and count job is pipelined to the max job. Here the key-value pair is formed by a dummy key "0"
      and the list of passengers with number of flights.

