from common.dataclass import BaseDataStructure, dataclass
from common.reducer import SequentialReducer


@dataclass
class MaxReducerInputStructure(BaseDataStructure):
    """Data structure expected during the reducing stage of the max function."""

    dummy_key: str
    pass_list: list

    def _validate_data(self):
        """Validate data as required."""
        if not self.dummy_key == "0":
            raise ValueError("Dummy Key is invalid")

        if not isinstance(self.pass_list, list):
            raise ValueError("Shuffler Error")


class MaxReducer(SequentialReducer):
    """Reducer class to define the groupby function."""

    def __init__(self, struct: dataclass) -> None:
        super().__init__(struct)

    def _validate_data(self, line: str) -> dataclass:
        # Strip and split line.
        line = line.strip().split(",", 1)

        # Convert data types as required.
        try:
            line[-1] = eval(line[-1])
        except:
            pass

        # Create an dataclass for the reducing function.
        try:
            return self.struct(*line)
        except ValueError as e:
            raise ValueError(f"{e} at Data Row: \n{line}")

    def _row_reducer(self, struct: dataclass) -> None:
        # Print output of reducer as required.
        values = list(map(lambda x: [x[0], int(x[1])], struct.pass_list))
        values = {k: v for (k, v) in values}
        max_value = max(values.values())

        for k, v in values.items():
            if v == max_value:
                print(f"Maximum number of trips ({v}) is done by {k}")


if __name__ == "__main__":
    reducer = MaxReducer(MaxReducerInputStructure)
    reducer()
