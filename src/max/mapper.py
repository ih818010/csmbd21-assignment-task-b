import multiprocessing as mp
import re

from common.dataclass import BaseDataStructure, dataclass
from common.mapper import ParallelMapper

N_CORES = mp.cpu_count()


@dataclass
class MaxMapperInputStructure(BaseDataStructure):
    """Data structure expected during the mapping stage of the max function."""

    passenger_id: str
    num_flights: int

    def _validate_data(self):
        """Validate data as required."""
        if not re.match(r"[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}$", self.passenger_id):
            raise ValueError("Invalid Passenger ID")

        if not isinstance(self.num_flights, int):
            raise ValueError("Invalid number of lights")


class MaxMapper(ParallelMapper):
    """Mapper class to define the max function."""

    def __init__(self, n_cores: int, struct: dataclass) -> None:
        super().__init__(n_cores, struct)

    def _validate_data(self, line: str) -> dataclass:
        # Strip and split line.
        line = line.strip().split(",")

        # Convert data types as required.
        try:
            line[-1] = int(line[-1])
        except:
            pass

        # Create an dataclass for the mapping function.
        try:
            return self.struct(*line)
        except ValueError as e:
            raise ValueError(f"{e} at Data Row: \n{line}")

    def _row_mapper(self, struct: dataclass) -> None:
        # Print output of mapper as required.
        print(f"0,[{struct.passenger_id},{struct.num_flights}]")


if __name__ == "__main__":
    mapper = MaxMapper(N_CORES, MaxMapperInputStructure)
    mapper()
