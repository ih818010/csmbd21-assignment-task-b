import re
from collections import defaultdict

from common.dataclass import BaseDataStructure, dataclass
from common.shuffler import Shuffler


@dataclass
class MaxShufflerInputStructure(BaseDataStructure):
    """Data structure expected during the shuffling stage of the max function."""

    dummy_key: str
    num_flights: list

    def _validate_data(self):
        """Validate data as required."""
        if self.dummy_key != "0":
            raise ValueError("Invalid Dummy Key")

        if not isinstance(self.num_flights, list):
            raise ValueError("Invalid mapping. Value should always be 1")


class MaxShuffler(Shuffler):
    """Shuffler class to define the max function."""

    def __init__(self, struct: BaseDataStructure) -> None:
        super().__init__(struct)

    def _validate_data(self, line: str) -> BaseDataStructure:
        # Strip and split line.
        line = line.strip().split(",", 1)
        try:
            line[-1] = line[-1][1:-1].split(",")

        # Convert data types as required.
        except Exception as e:
            raise Exception(e)

        # Create an dataclass for the reducing function.
        try:
            return self.struct(*line)
        except Exception as e:
            raise Exception(f"{e} at row: \n{line}")

    def _row_shuffler(self, struct: BaseDataStructure) -> None:
        # Print output of shuffler as required.
        self._data[struct.dummy_key].append(struct.num_flights)


if __name__ == "__main__":
    shuffler = MaxShuffler(MaxShufflerInputStructure)
    shuffler()
