"""Template class for reducing the sorted key-value pairs using an aggregation."""

import multiprocessing as mp
import sys
from abc import ABC, abstractmethod
from typing import Any

import numpy as np

from common.dataclass import BaseDataStructure


class BaseReducer(ABC):
    """Template class for a reducer."""

    def __init__(self, struct: BaseDataStructure) -> None:
        """Init function

        Args:
            struct (BaseDataStructure): Input data structure.
        """
        self.input_ = list(sys.stdin)
        self.struct = struct

    @classmethod
    @abstractmethod
    def reducer(self) -> None:
        """Abstract method to specify aggregation logic for reduction."""
        pass

    @classmethod
    @abstractmethod
    def _validate_data(self, line: str) -> BaseDataStructure:
        """
        Abstract method to convert data types.
        This is required as the input from `sys.stdin` is always read as a string.
        """
        pass

    @classmethod
    @abstractmethod
    def _row_reducer(self, struct: BaseDataStructure) -> None:
        """Abstract method to define aggregation to be applied to each row in the input data."""
        pass

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self.reducer()


class SequentialReducer(BaseReducer):
    """Template class for a Sequential Mapper."""

    def __init__(self, struct: BaseDataStructure) -> None:
        super().__init__(struct)

    def reducer(self) -> None:
        """
        Reduces the input into key-value pairs in the following steps -
        1. Checks the data structure for any discrepencies.
        2. Applies the aggregation function `_row_reducer` to each row.

        This method is defined in the template because the steps for sequentially reducing is same,
        while the reducing function may be different.

        """
        for line in self.input_:
            struct = self._validate_data(line)
            self._row_reducer(struct)


class ParallelReducer(BaseReducer):
    """Template class for a Parallel Mapper"""

    def __init__(self, n_cores: int, struct: BaseDataStructure) -> None:
        """Init Function

        Args:
            n_cores (int): Number of cores available for reducing.
            struct (BaseDataStructure): The input data structure for the mapper.
        """
        super().__init__(struct)
        self.n_cores = n_cores

        # Use the np.array_split to chunk the input.
        # This is so that all the data is used even if the chunk sizes are different.
        self._chunks = np.array_split(self.input_, n_cores)

    def _chunk_reducer(self, chunk: np.array) -> None:
        """Aggregation function for each chunk. Aggregation is done in 2 steps -
        1. Check the data structure for any discrepencies.
        2. Apply the map function to each row.

        Args:
            chunk (np.array): Chunk to be processed by a single core/process.
        """
        for line in chunk:
            struct = self._validate_data(line)
            self._row_reducer(struct)

    def reducer(self) -> None:
        """
        Reduces the input in the following steps -
        1. Create a multi-processing pool.
        2. Use the inbuilt mp.Pool.map() function to process chunks in parallel.
        3. Close the pool when the job is done.

        This method is defined in the template because the steps for parallel reducing is same,
        while the reducing function may be different.

        """
        pool = mp.Pool(processes=self.n_cores)  # Create pool
        pool.map(self._chunk_reducer, self._chunks)  # Parallel map
        pool.close()  # Close pool
        pool.join()  # Ensure that the next step is done after all the processes in the pool is complete.
