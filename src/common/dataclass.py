"""
Template classes for validation of data types.
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass, fields


@dataclass(kw_only=True)
class BaseDataStructure(ABC):
    """
    Base Template for dataclass.

    Validates -
    1. Input data types.
    2. Input data quality.
    """

    def _validate_types(self) -> None:
        """
        Function to validate datatypes.

        Raises:
            TypeError: Raise when the type of a field does not match the expected type.
        """
        # For each data field, compare actual and expected data type, raising error when not the same.
        for field in fields(self):
            attr = getattr(self, field.name)
            if not isinstance(attr, field.type):
                raise TypeError(
                    f"Field {field.name} is of type {type(attr)}. Expected type is {field.type}"
                )
        return

    @classmethod
    @abstractmethod
    def _validate_data(self) -> None:
        """Abstract method to specify the data quality expected at each step."""
        pass

    def __post_init__(self) -> None:
        self._validate_types()
        self._validate_data()
