"""Template class for mapping input data into key-value pairs."""
import multiprocessing as mp
import sys
from abc import ABC, abstractmethod
from typing import Any

import numpy as np

from common.dataclass import BaseDataStructure


class BaseMapper(ABC):
    """Base Template for mapper. Defined as an abstract class."""

    def __init__(self, struct: BaseDataStructure) -> None:
        """Initializer

        Args:
            struct (BaseDataStructure): Expected input data structure.
        """
        self.input_ = list(sys.stdin)
        self.struct = struct

    @classmethod
    @abstractmethod
    def mapper(self) -> None:
        """Abstract method to specify mapping procedure."""
        pass

    @classmethod
    @abstractmethod
    def _validate_data(self, line: str) -> BaseDataStructure:
        """
        Abstract method to convert data types.
        This is required as the input from `sys.stdin` is always read as a string.
        """
        pass

    @classmethod
    @abstractmethod
    def _row_mapper(self, struct: BaseDataStructure) -> None:
        """Abstract method to define function to be applied to each row in the input data."""
        pass

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self.mapper()


class SequentialMapper(BaseMapper):
    """Template class for a Sequential Mapper."""

    def __init__(self, struct: BaseDataStructure) -> None:
        super().__init__(struct)

    def mapper(self) -> None:
        """
        Maps the input into key-value pairs in the following steps -
        1. Checks the data structure for any discrepencies.
        2. Applies the map function to each row.

        This method is defined in the template because the steps for sequentially mapping is same,
        while the mapping function may be different.

        """
        _ = list(
            map(lambda line: self._row_mapper(self._validate_data(line)), self.input_)
        )


class ParallelMapper(BaseMapper):
    """Template class for a Parallel Mapper"""

    def __init__(self, n_cores: int, struct: BaseDataStructure) -> None:
        """Init Function

        Args:
            n_cores (int): Number of cores available for mapping.
            struct (BaseDataStructure): The input data structure for the mapper.
        """
        super().__init__(struct)
        self.n_cores = n_cores

        # Use the np.array_split to chunk the input.
        # This is so that all the data is used even if the chunk sizes are different.
        self._chunks = np.array_split(self.input_, n_cores)

    def _chunk_mapper(self, chunk: np.array) -> None:
        """Mapping function for each chunk. Mapping is done in 2 steps -
        1. Check the data structure for any discrepencies.
        2. Apply the map function to each row.

        Args:
            chunk (np.array): Chunk to be processed by a single core/process.
        """
        for line in chunk:
            struct = self._validate_data(line)
            self._row_mapper(struct)

    def mapper(self) -> None:
        """
        Maps the input into key-value pairs in the following steps -
        1. Create a multi-processing pool.
        2. Use the inbuilt mp.Pool.map() function to process chunks in parallel.
        3. Close the pool when the job is done.

        This method is defined in the template because the steps for parallel mapping is same,
        while the mapping function may be different.

        """
        pool = mp.Pool(processes=self.n_cores)  # Create pool
        pool.map(self._chunk_mapper, self._chunks)  # Parallel map
        pool.close()  # Close pool
        pool.join()  # Ensure that the next step is done after all the processes in the pool is complete.
