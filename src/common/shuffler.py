"""Template class for shuffling and sorting the key-value pairs."""

import sys
from abc import ABC, abstractmethod
from collections import defaultdict
from typing import Any

from common.dataclass import BaseDataStructure


class Shuffler(ABC):
    """Template class for a shuffler."""

    def __init__(self, struct: BaseDataStructure) -> None:
        """Init function

        Args:
            struct (BaseDataStructure): Input structure expected by the shuffler.
        """
        self._input = list(sys.stdin)
        self.struct = struct
        self._data = defaultdict(
            list
        )  # defaultdict(list) to organise the mapper output.

    @classmethod
    @abstractmethod
    def _validate_data(self, line: str) -> BaseDataStructure:
        """
        Abstract method to convert data types.
        This is required as the input from `sys.stdin` is always read as a string.
        """
        pass

    @classmethod
    @abstractmethod
    def _row_shuffler(self, struct: BaseDataStructure) -> None:
        """Abstract method to specify shuffling procedure."""
        pass

    def shuffler(self):
        """
        We want to shuffle the data so that all the values of a single key is appended to a list.

        The steps for shuffling is the same -
        1. Validate the data.
        2. Aplly shuffling function to each input row.

        """
        for line in self._input:
            struct = self._validate_data(line)
            self._row_shuffler(struct)

        for k, v in sorted(self._data.items()):
            print(f"{k},{v}")

        return

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self.shuffler()
