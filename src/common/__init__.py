"""
Template classes for - 
1. DataClass - Template class for validation of data types.
2. Mapper    - Template class for mapping input data into key-value pairs.
3. Shuffler  - Template class for shuffling and sorting the key-value pairs.
4. Reducer   - Template class for reducing the sorted key-value pairs using an aggregation.
"""
