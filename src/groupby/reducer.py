import multiprocessing as mp
import re

from common.dataclass import BaseDataStructure, dataclass
from common.reducer import ParallelReducer

N_CORES = mp.cpu_count()


@dataclass
class GroupByReducerInputStructure(BaseDataStructure):
    """Data structure expected during the reducing stage of the groupby function."""

    passenger_id: str
    num_flights: list

    def _validate_data(self):
        """Validate data as required."""
        if not re.match(r"[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}$", self.passenger_id):
            raise ValueError("Invalid Passenger ID")

        if not set(self.num_flights) == {1}:
            raise ValueError("Invalid number of flights")


class GroupByReducer(ParallelReducer):
    """Reducer class to define the groupby function."""

    def __init__(self, n_cores: int, struct: dataclass) -> None:
        super().__init__(n_cores, struct)

    def _validate_data(self, line: str) -> dataclass:
        # Strip and split line.
        line = line.strip().split(",", 1)

        # Convert data types as required.
        try:
            line[-1] = eval(line[-1])
            line[-1] = list(map(lambda x: int(x), line[-1]))
        except:
            pass

        # Create an dataclass for the reducing function.
        try:
            return self.struct(*line)
        except ValueError as e:
            raise ValueError(f"{e} at Data Row: \n{line}")

    def _row_reducer(self, struct: dataclass) -> None:
        # Print output of reducer as required.
        print(f"{struct.passenger_id},{sum(struct.num_flights)}")


if __name__ == "__main__":
    reducer = GroupByReducer(n_cores=N_CORES, struct=GroupByReducerInputStructure)
    reducer()
