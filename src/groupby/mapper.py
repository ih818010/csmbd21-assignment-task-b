import multiprocessing as mp
import re

from common.dataclass import BaseDataStructure, dataclass
from common.mapper import ParallelMapper

N_CORES = mp.cpu_count()


@dataclass
class GroupByMapperInputStructure(BaseDataStructure):
    """Data structure expected during the mapping stage of the groupby function."""

    passenger_id: str
    flight_id: str
    source_airp: str
    dest_airp: str
    dep_time: int
    flight_time: int

    def _validate_data(self):
        """Validate the input data as specified in the data description."""
        if not re.match(r"[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}$", self.passenger_id):
            raise ValueError("Invalid Passenger ID")

        if not re.match(r"[A-Z]{3}[0-9]{4}[A-Z]{1}$", self.flight_id):
            raise ValueError("Invalid Flight ID")

        if not re.match(r"[A-Z]{3}$", self.source_airp):
            raise ValueError("Invalid Source Airport")

        if not re.match(r"[A-Z]{3}$", self.dest_airp):
            raise ValueError("Invalid Destination Airport")

        if not re.match(r"^[0-9]{10}$", str(self.dep_time)):
            raise ValueError("Invalid Departure Time")

        if not re.match(r"^\d{1,4}$", str(self.flight_time)):
            raise ValueError("Invalid Flight Time")


class GroupByMapper(ParallelMapper):
    """Mapper class to define the groupby function."""

    def __init__(self, n_cores: int, struct: dataclass) -> None:
        super().__init__(n_cores, struct)

    def _validate_data(self, line: str) -> dataclass:
        # Strip and split line.
        line = line.strip().split(",")

        # Convert data types as required.
        try:
            line[-2] = int(line[-2])
            line[-1] = int(line[-1])
        except:
            pass

        # Create an dataclass for the mapping function.
        try:
            return self.struct(*line)
        except ValueError as e:
            raise ValueError(f"{e} at Data Row: \n{line}")

    def _row_mapper(self, struct: dataclass) -> None:
        # Print output of mapper as required.
        print(f"{struct.passenger_id},{1}")


if __name__ == "__main__":
    mapper = GroupByMapper(n_cores=N_CORES, struct=GroupByMapperInputStructure)
    mapper()
