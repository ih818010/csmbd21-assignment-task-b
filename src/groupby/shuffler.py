import re
from collections import defaultdict

from common.dataclass import BaseDataStructure, dataclass
from common.shuffler import Shuffler


@dataclass
class GroupByShufflerInputStructure(BaseDataStructure):
    """Data structure expected during the shuffling stage of the groupby function."""

    passenger_id: str
    num_flights: int

    def _validate_data(self):
        """Validate data as required."""
        if not re.match(r"[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}$", self.passenger_id):
            raise ValueError("Invalid Passenger ID")

        if self.num_flights != 1:
            raise ValueError("Invalid mapping. Value should always be 1")


class GroupByShuffler(Shuffler):
    """Shuffler class to define the groupby function."""

    def __init__(self, struct: BaseDataStructure) -> None:
        super().__init__(struct)

    def _validate_data(self, line: str) -> BaseDataStructure:
        # Strip and split line.
        line = line.strip().split(",")

        # Convert data types as required.
        try:
            line[1] = int(line[1])
        except Exception as e:
            raise Exception(e)

        # Create an dataclass for the reducing function.
        try:
            return self.struct(*line)
        except Exception as e:
            raise Exception(f"{e} at row: \n{line}")

    def _row_shuffler(self, struct: BaseDataStructure) -> None:
        # Print output of shuffler as required.
        self._data[struct.passenger_id].append(struct.num_flights)


if __name__ == "__main__":
    shuffler = GroupByShuffler(GroupByShufflerInputStructure)
    shuffler()
