"""
Classes for performing a groupby count function using map-reduce.

1. Mapper    - Class for mapping input data into key-value pairs.
2. Shuffler  - Class for shuffling and sorting the key-value pairs.
3. Reducer   - Class for reducing the sorted key-value pairs using an sum to count.
"""
