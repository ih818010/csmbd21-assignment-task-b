from common.dataclass import BaseDataStructure, dataclass
from common.reducer import SequentialReducer


@dataclass
class DropDuplicatesReducerInputStructure(BaseDataStructure):
    """Data structure expected during the reducing stage of the drop duplicates function."""

    hash_id: str
    row_str: str

    def _validate_data(self):
        """Validate data as required."""
        if not isinstance(self.hash_id, str):
            raise ValueError(
                "Expected value for hash_id is string, got {}".format(
                    type(self.hash_id)
                )
            )

        if not isinstance(self.row_str, str):
            raise ValueError(
                "Expected value for row_str is string, got {}".format(
                    type(self.row_str)
                )
            )


class DropDuplicatesReducer(SequentialReducer):
    """Reducer class to define the groupby function."""

    def __init__(self, struct: dataclass) -> None:
        super().__init__(struct)

    def _validate_data(self, line: str) -> dataclass:
        # Strip and split line.
        line = line.strip().split(",", 1)

        # Create an dataclass for the reducing function.
        try:
            return self.struct(*line)
        except ValueError as e:
            raise ValueError(f"{e} at Data Row: \n{line}")

    def _row_reducer(self, struct: dataclass) -> None:
        # Print output of reducer as required.
        print(*set(eval(struct.row_str)))


if __name__ == "__main__":
    reducer = DropDuplicatesReducer(DropDuplicatesReducerInputStructure)
    reducer()
