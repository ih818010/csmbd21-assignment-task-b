import re
from collections import defaultdict

from common.dataclass import BaseDataStructure, dataclass
from common.shuffler import Shuffler


@dataclass
class DropDuplicatesShufflerInputStructure(BaseDataStructure):
    """Data structure expected during the shuffling stage of the drop duplicates function."""

    hash_id: str
    row_str: str

    def _validate_data(self):
        """Validate data as required."""
        if not isinstance(self.hash_id, str):
            raise ValueError(
                "Expected value for hash_id is string, got {}".format(
                    type(self.hash_id)
                )
            )

        if not isinstance(self.row_str, str):
            raise ValueError(
                "Expected value for row_str is string, got {}".format(
                    type(self.row_str)
                )
            )


class DropDuplicatesShuffler(Shuffler):
    """Shuffler class to define the drop duplicates function."""

    def __init__(self, struct: BaseDataStructure) -> None:
        super().__init__(struct)

    def _validate_data(self, line: str) -> BaseDataStructure:
        # Strip and split line.
        line = line.strip().split(",", 1)

        # Create an dataclass for the reducing function.
        try:
            return self.struct(*line)
        except Exception as e:
            raise Exception(f"{e} at row: \n{line}")

    def _row_shuffler(self, struct: BaseDataStructure) -> None:
        # Print output of shuffler as required.
        self._data[struct.hash_id].append(struct.row_str)


if __name__ == "__main__":
    shuffler = DropDuplicatesShuffler(DropDuplicatesShufflerInputStructure)
    shuffler()
