import hashlib
import multiprocessing as mp

from common.dataclass import BaseDataStructure, dataclass
from common.mapper import SequentialMapper

N_CORES = mp.cpu_count()


@dataclass
class DropDuplicatesMapperInputStructure(BaseDataStructure):
    """Data structure expected during the mapping stage of the drop duplicates function."""

    row_str: str

    def _validate_data(self):
        """Validate the input data as specified in the data description."""
        if not isinstance(self.row_str, str):
            raise ValueError(
                "Expected str input. Instead got {}".format(type(self.row_str))
            )


class DropDuplicatesMapper(SequentialMapper):
    """Mapper class to define the drop duplicates function."""

    def __init__(self, struct: dataclass) -> None:
        super().__init__(struct)

    def _validate_data(self, line: str) -> dataclass:
        # Strip and split line.
        line = [line.strip()]

        # Create an dataclass for the mapping function.
        try:
            return self.struct(*line)
        except ValueError as e:
            raise ValueError(f"{e} at Data Row: \n{line}")

    def _row_mapper(self, struct: dataclass) -> None:
        # Print output of mapper as required.
        self._hasher = hashlib.md5()
        self._hasher.update(struct.row_str.encode("utf-8"))
        print(f"{self._hasher.hexdigest()},{struct.row_str}")


if __name__ == "__main__":
    mapper = DropDuplicatesMapper(struct=DropDuplicatesMapperInputStructure)
    mapper()
