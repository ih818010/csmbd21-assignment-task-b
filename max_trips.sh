# Shell script to run the map reduce job.

clear
export PYTHONPATH="${PYTHONPATH}:$(pwd)/src"    # Add src to PYTHONPARH

cat ./data/AComp_Passenger_data_no_error\(1\).csv \
    | python3 src/dropduplicates/mapper.py \
    | python3 src/dropduplicates/shuffler.py \
    | python3 src/dropduplicates/reducer.py \
    | python3 src/groupby/mapper.py \
    | python3 src/groupby/shuffler.py \
    | python3 src/groupby/reducer.py \
    | python3 src/max/mapper.py \
    | python3 src/max/shuffler.py \
    | python3 src/max/reducer.py
